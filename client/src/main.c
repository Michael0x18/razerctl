#include "razerctl.h"

void help(void)
{
	fputs(BOLD_ON, stdout);
	puts(VERSION_STRING);
}

int main(int argc, char **argv)
{
	if (argc == 1)
	{
		help();
	}
}
