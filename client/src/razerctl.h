#ifndef razerctl_h
#define razerctl_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#define COLOR_RED "\033[0;31m"
#define COLOR_GREEN "\x1B[32m"
#define COLOR_BLUE "\x1B[34m"
#define COLOR_RESET "\033[0m"
#define BOLD_ON "\033[1m"
#define BOLD_OFF "\033[21m"

#define VERSION_STRING "razerctl v0.0 DEVELOPMENT"

void help(void);
int main(int argc, char **argv);

#endif