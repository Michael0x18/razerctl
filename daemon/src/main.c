#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

void handle_hup(int sig){
}

static void skeleton_daemon(){
	pid_t pid;
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);
	if (pid > 0)
		exit(EXIT_SUCCESS);
	if (setsid() < 0)
		exit(EXIT_FAILURE);
	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, handle_hup);
	pid = fork();
	if (pid < 0)
		exit(EXIT_FAILURE);
	if (pid > 0)
		exit(EXIT_SUCCESS);
	umask(0);
	chdir("/usr/local/razerctl");
	int x;
	for (x = sysconf(_SC_OPEN_MAX); x>=0; x--){
		close (x);
	}
	openlog ("razerctl-daemon", LOG_PID, LOG_DAEMON);
}

int main(){
	skeleton_daemon();

	while (1){
		
		
		syslog (LOG_NOTICE, "razerctl-daemon started");
		sleep (20);
		break;
	}

	syslog (LOG_NOTICE, "razerctl-daemon stopped");
	closelog();

	return EXIT_SUCCESS;
}
